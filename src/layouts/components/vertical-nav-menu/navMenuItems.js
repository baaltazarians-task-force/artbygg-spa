/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
import { required as CompanyPermissions } from '@/router/routes/Company'
import { required as UserPermissions } from '@/router/routes/User'


export default [
  {
    url: null,
    name: "Company",
    slug: "companies",
    icon: "HomeIcon",
    i18n: 'companies',
    permissions: CompanyPermissions.main,
    submenu: [
      {
        url: "/company/new",
        name: "Add new company",
        slug: "company_create",
        i18n: 'create_user',
        permissions: CompanyPermissions.add
      },
      {
        url: "/company/list",
        name: "Manage companies",
        slug: "company_manage",
        i18n: 'create_user',
        permissions: CompanyPermissions.list
      }
    ]
  },
  {
    url: null,
    name: "User",
    slug: "users",
    icon: "UserIcon",
    i18n: 'user',
    permissions: UserPermissions.main,
    submenu:[
      {
        url: "/user/new",
        name: "Create new user",
        slug: "create_user",
        i18n: 'create_user',
        permissions: UserPermissions.add
      },
      {
        url: "/user/list",
        name: "Manage users",
        slug: "user_list",
        i18n: 'user_list',
        permissions: UserPermissions.list
      }
    ]
  },
 
]


