import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import store from '../store/index'

Vue.use(VueRouter);

const Router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: () => { return { x: 0, y: 0 } },
  routes
})

// Remove loading screen when page is loaded
Router.afterEach(() => {
  const el = document.getElementById('loading-bg');
  if (el) el.style.display = 'none';
});

/*
---- PERMISSION MIDDLEWARE
 */
Router.beforeEach((to, from, next) => {
  const user           = store.state.auth.user;
  const groups         = store.getters['auth/getUserGroups'];
  const permissions    = store.getters['auth/getUserPermissions'];
  const tokenPayload   = store.getters['auth/getTokenPayload'];
  const expiration     = Math.floor(new Date().getTime() / 10000);
  const userLoggedIn   = () => tokenPayload && tokenPayload.exp && tokenPayload.exp > expiration - 40
  const passwordChange = () => store.state.auth.requirePasswordChange;

  // go to password reset page
  if (to.name == 'auth-reset-password-handle' || to.name == 'auth-reset-password') {
    return next();
  }

  // redirect to login page if user is not logged in (check if token exists and has not expired)
  if (!userLoggedIn() && (from.name !== 'auth-login' || to.name !== 'auth-login') && to.name !== 'auth-login') {
    return next({ name: 'auth-login' });
  }

  // check if user must change password on logon
  if (passwordChange() && (from.name !== 'auth-change-password' || to.name !== 'auth-change-password') && to.name !== 'auth-change-password') {
    return next({ name: 'auth-change-password' });
  }

  // prohibit accessing change password on logon page without reason
  if (to.name == 'auth-change-password' && !passwordChange()) {
    return next({ name: 'error-404' });
  }

  // home can be accesed by everyone
  if (to.name === 'home') {
    return next();
  }

  // check if user has permission to access route
  return next();
});

export default Router;