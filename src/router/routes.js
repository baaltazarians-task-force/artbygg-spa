import AuthRoutes from './routes/Auth'
import UserRoutes from './routes/User'
import CompanyRoutes from './routes/Company'
import * as _other from './routes/_other'

export default
[
  {
    path: '',
    component: () => import('../layouts/main/Main.vue'),
    children: [
      UserRoutes,
      CompanyRoutes,
      _other.PageHome
    ],
  },
  {
    path: '',
    component: () => import('@/layouts/full-page/FullPage.vue'),
    children: [
      AuthRoutes,
      _other.Page404
    ]
  },
];
