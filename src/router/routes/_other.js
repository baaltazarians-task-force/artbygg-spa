export const PageHome = {
  path: '/home',
  name: 'home',
  component: () => import('@/views/other/Home.vue')
}

export const Page404 = {
  path: '*',
  name: 'error-404',
  component: () => import('@/views/other/404.vue')
}