

export default
{
  path: '/auth',
  component: () => import('@/views/Auth/Main'),
  children: [
    {
      path: '/',
      name: 'auth',
      redirect: { name: 'auth-login' }
    },
    {
      path: 'login',
      name: 'auth-login',
      component: () => import('@/views/Auth/Login.vue'),
    },
    {
      path: 'change-password',
      name: 'auth-change-password',
      component: () => import('@/views/Auth/ChangePassword.vue'),
    },
    {
      path: 'reset-password',
      name: 'auth-reset-password',
      component: () => import('@/views/Auth/ResetPasswordForm.vue'),
    },
    {
      path: 'reset-password/:token',
      name: 'auth-reset-password-handle',
      component: () => import('@/views/Auth/ResetPasswordHandle.vue'),
    }
  ],
}