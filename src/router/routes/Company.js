import PermissionGroup from '@/classes/Permission/PermissionGroup'
import Permission from '@/classes/Permission/Permission'

export const required = {
  main: new PermissionGroup([
    new Permission(undefined, 'company.list',    'or'),
    new Permission(undefined, 'company.manage',  'or'),
    new Permission(undefined, 'company.create',  'or')
  ]),
  list: new PermissionGroup([
    new Permission(undefined, 'company.list',    'or'),
    new Permission(undefined, 'company.manage',  'or'),
  ]),
  add: new PermissionGroup([
    new Permission(undefined, 'company.create',  'and')
  ]),
}

export default
{
  path: '/company',
  component: () => import('@/views/Company/Main.vue'),
  meta: { permissions: required.main },
  children: [
    {
      path: '/',
      name: 'company',
      redirect: { name: 'company-list' }
    },
    {
      path: 'list',
      name: 'company-list',
      component: () => import('@/views/Company/List.vue'),
      meta: { permissions: required.list }
    },
    {
      path: 'new',
      name: 'company-new',
      component: () => import('@/views/Company/New.vue'),
      meta: { permissions: required.add }
    }
  ],
}