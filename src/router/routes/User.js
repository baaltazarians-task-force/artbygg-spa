import PermissionGroup from '@/classes/Permission/PermissionGroup'
import Permission from '@/classes/Permission/Permission'

export const required = {
  main: new PermissionGroup([
    new Permission(undefined, 'user.list',    'or'),
    new Permission(undefined, 'user.manage',  'or'),
    new Permission(undefined, 'user.create',  'or')
  ]),
  list: new PermissionGroup([
    new Permission(undefined, 'user.list',    'or'),
    new Permission(undefined, 'user.manage',  'or'),
  ]),
  add: new PermissionGroup([
    new Permission(undefined, 'user.create',  'and')
  ]),
}

export default
{
  path: '/user',
  component: () => import('@/views/User/Main.vue'),
  meta: { permissions: required.main },
  children: [
    {
      path: '/',
      name: 'user',
      redirect: { name: 'user-list' }
    },
    {
      path: 'list',
      name: 'user-list',
      component: () => import('@/views/User/List.vue'),
      meta: { permissions: required.list }
    },
    {
      path: 'new',
      name: 'user-new',
      component: () => import('@/views/User/New.vue'),
      meta: { permissions: required.add }
    }
  ],
}