import Service from './Service'

export default class UserService extends Service {
  constructor (axios, hostname) {
    super();
    this.axios = axios;
    this.hostname = `${hostname}/user`;
  }

  getUsers (page, limit, search, relation = [], attribute = [], filter = [])
  {
    return this.axios.get(`${this.hostname}/list${this.getParams({ page, limit, search, relation, attribute, filter })}`);
  }

  createUser (data)
  {
    return this.axios.put(`${this.hostname}`, { ...data });
  }

  changeStatus (userId, changeStatus)
  {
    return this.axios.patch(`${this.hostname}/${userId}`, { changeStatus });
  }
}
