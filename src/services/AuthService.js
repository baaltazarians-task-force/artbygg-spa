export default class AuthService {
  constructor (axios, hostname) {
    this.axios = axios;
    this.hostname = `${hostname}/authorization`;
  }

  authorize (email, password) {
    return this.axios.post(`${this.hostname}/login`, { email, password });
  }

  logout () {
    return this.axios.get(`${this.hostname}/logout`);
  }

  getPermissions () {
    return this.axios.get(`${this.hostname}/user-permissions`);
  }

  getUserInfo () {
    return this.axios.get(`${this.hostname}/user-info`);
  }

  changePassword (password, token) {
    return token ?
        this.axios.post(`${this.hostname}/password-reset/${token}`, { password })
      : this.axios.post(`${this.hostname}/change-password`, { password });
  }

  checkPasswordResetToken (token) {
    return this.axios.get(`${this.hostname}/password-reset/${token}`);
  }

  resetPassword (email) {
    return this.axios.post(`${this.hostname}/password-reset`, { email });
  }
}
