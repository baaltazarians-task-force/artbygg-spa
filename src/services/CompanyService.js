import Service from './Service'

export default class CompanyService extends Service {
  constructor (axios, hostname) {
    super();
    this.axios = axios;
    this.cpHostname = `${hostname}/company`;
    this.wpHostname = `${hostname}/work-position`
  }

  getCompanies (page, limit, search, relation = [], attribute = [])
  {
    return this.axios.get(`${this.cpHostname}/list${this.getParams({ page, limit, search, relation, attribute })}`);
  }

  getWorkPlaces (page, limit, search, relation = [], attribute = [])
  {
    return this.axios.get(`${this.wpHostname}/list${this.getParams({ page, limit, search, relation, attribute })}`);
  }
}