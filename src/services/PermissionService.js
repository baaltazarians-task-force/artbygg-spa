import Service from './Service'

export default class PermissionService extends Service {
  constructor (axios, hostname) {
    super();
    this.axios = axios;
    this.hostname = `${hostname}/permission`;
  }

  getPermissions ()
  {
    return this.axios.get(`${this.hostname}/list`);
  }
}
