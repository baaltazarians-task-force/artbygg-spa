export default class NotifyService {
  constructor (axios, hostname) {
    this.axios = axios;
    this.hostname = `${hostname}/notification`;
  }

  loadNotifications () {
    return this.axios.get(`${this.hostname}/list`);
  }
}
