import Service from "./Service";

export default class GroupService extends Service {
  constructor (axios, hostname) {
    super();
    this.axios = axios;
    this.hostname = `${hostname}/group`;
  }

  getGroups (page, limit, search, relation = [], attribute = [])
  {
    return this.axios.get(`${this.hostname}/list${this.getParams({ page, limit, search, relation, attribute })}`);
  }

  createGroup (data)
  {
    return this.axios.put(`${this.hostname}`, { ...data });
  }

  updateGroup (data, id)
  {
    return this.axios.patch(`${this.hostname}/${id}`, { ...data });
  }

  deleteGroup (id)
  {
    return this.axios.delete(`${this.hostname}/${id}`);
  }
}