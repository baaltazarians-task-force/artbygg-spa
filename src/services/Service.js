export default class Service {
  constructor () {}

  getParams (input) {
    let params = '?';
    const keys = Object.keys(input);
    keys.forEach(key => {
      if (Array.isArray(input[key]))
        input[key].forEach(a => {
          params += a ? `${key}[]=${a}` : '';
        })
      else
        params += input[key] ? `${key}=${input[key]}&` : '';
    });
    return params === "?" ? '' : params.replace(/[&\/]*$/, '');
  }
}