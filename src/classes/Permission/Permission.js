import store from '@/store/index'

export default class Permission {

  /**
   * 
   * @param {number} id
   * @param {string} slug 
   */
  constructor (id, slug, type = 'and')
  {
    this.id   = id;
    this.slug = slug;
    this.type = type;
  }

  // checks if logged in user has this permission
  check ()
  {
    const user = store.state.auth.user;
    return user ? user.permissions.some(p => p.has(this.group, this.name)) : false;
  }

  has (group, name)
  {
    return this.group === group && this.name === name;
  }
}
