import Permission from './Permission'

export default class PermissionGroup {

  /**
   * contructor
   * @param {Array<Permission>} permissions 
   */
  constructor (permissions = []) {
    this._permissions = permissions;
  }

  get permissions () {
    return this._permissions;
  }

  /**
   * Check if all required permission are met
   * @param {Array<Permission>} against 
   */
  check (against = [])
  {
    return PermissionGroup.fastCheck(against, this.permissions);
  }

  /**
   * Check if all required permission are met
   * @param {Array<Permission>} against 
   * @param {Array<Permission>} required 
   */
  static fastCheck (against = [], required = [])
  {
    if (
      !against.length ||
      !required.length
    ) return true;

    let toPass = 0;
    let passed = 0;

    required.forEach(e => {
      if (e.type === 'and') toPass++;

      if (against.find(p => p.slug === e.slug || p.id === e.id)) {
        passed++;
      }
    });
    
    toPass = !toPass ? 1 : toPass;
    
    return passed >= toPass;
  }
}