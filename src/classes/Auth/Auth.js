import Permission from '@/classes/Permission/Permission'
import store from '@/store/index'
import User from '../User/User'

export default class Auth {

  /**
   * @param {User} user
   */
  constructor (user)
  {
    this.user = user;
  }

  get groups ()
  {
    return store.state.auth.groups;
  }

  get permissions ()
  {
    return store.state.auth.permissions;
  }

  get initials () {
    return this.firstName && this.lastName ? this.firstName[0]+this.lastName[0] : null;
  }
}
