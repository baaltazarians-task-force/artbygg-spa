export default class Group {

  /**
   * 
   * @param {String} slug 
   * @param {String} description 
   */
  constructor (id, slug, description, permissions = [])
  {
    this.id = id;
    this.slug = slug;
    this.description = description;
    this.permissions = permissions;
  }
}
