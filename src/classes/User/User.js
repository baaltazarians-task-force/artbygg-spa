import store from '@/store/index'

export default class User {
  constructor (id, firstName, lastName, email, phone, status = false)
  {
    this.id        = id;
    this.firstName = firstName;
    this.lastName  = lastName;
    this.email     = email;
    this.phone     = phone;
    this.status    = status;
  }

  // get company ()
  // {
  //   return this.companyId ? store.getters['company/getCompanyById'](this.companyId) : null;
  // }

  // get groups ()
  // {
  //   return this.groupIds.map(g => store.getters['group/getGroupById'](g) || null);
  // }
}