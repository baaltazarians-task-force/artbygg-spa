export const addCompany    = (state, company) => state.companies.push(company);
export const clearCompanies = (state) => state.companies = [];

export const addWorkPlace    = (state, workPlace) => state.workPlaces.push(workPlace);
export const clearWorkPlaces = (state) => state.workPlaces = [];
