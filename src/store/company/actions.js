import Vue from 'vue'

export const loadCompanies = ({ rootState, commit }) => {
  commit('clearCompanies');
  rootState.api.company.getCompanies()
    .then(out => {
      out.data.response.forEach(e => commit('addCompany', e));
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load companies list'
      })
    })
}

export const loadWorkPlaces = ({ rootState, commit }) => {
  commit('clearWorkPlaces');
  rootState.api.company.getWorkPlaces()
    .then(out => {
      out.data.response.forEach(e => commit('addWorkPlace', {
        ...e,
        // temporary solution to convert slug into name, will be replaced with translation module
        slug: e.slug.split('_').map(e => e.charAt(0).toUpperCase() + e.slice(1)).join(' ')
      }));
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load work places list'
      })
    })
}
