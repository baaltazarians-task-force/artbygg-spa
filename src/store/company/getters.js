export const getCompanyById = (state) => (companyId) => {
  return state.companies.find(c => c.id === companyId) || null;
}

export const getSelectCompanies = (state) => {
  return state.companies.map(g => {
    return {
      label: g.name,
      value: g.id
    }
  })
}

export const getSelectWorkPlaces = (state) => {
  return state.workPlaces.map(g => {
    return {
      label: g.slug,
      value: g.id
    }
  })
}
