export const addPermission    = (state, permission) => state.permissions.push(permission);
export const clearPermissions = (state) => state.permissions = [];
