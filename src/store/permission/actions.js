import Vue from 'vue'

export const loadPermissions = ({ rootState, commit }) => {
  commit('clearPermissions');
  rootState.api.permission.getPermissions()
    .then(out => {
      out.data.response.forEach(e => commit('addPermission', e));
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load permissions list'
      })
    })
}