
export const clearNotifications = (state) => state.notifications = [];
export const addNotification = (state, notification) => state.notifications.push(notification);