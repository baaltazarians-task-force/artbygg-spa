import Vue from 'vue'

export const loadNotifications = ({ rootState, commit }) => {
  rootState.api.notify.loadNotifications()
    .then(out => {
      commit('clearNotifications');
      out.data.result.forEach(e => { commit('addNotification', e); });
    })
    .catch(error => {
      console.error(error);
    })
}

export const clearNotification = ({ rootState, state }, payload = []) => {
  
}
