import Vue from 'vue';
import Router from '../../router/index'

import Auth from '@/classes/Auth/Auth'
import User from '@/classes/User/User'
import Permission from '@/classes/Permission/Permission'

// authorize user be given (state) email and password
export const login = ({ rootState, state, commit, dispatch }) => {
  commit('loading', true);

  rootState.api.auth.authorize(state.loginForm.email, state.loginForm.password)
    .then(out => {
      // set token
      commit('setToken', out.data.token);
      commit('requirePasswordChange', out.data.password_change || false);

      dispatch('loadPermissions');
      dispatch('loadUserInfo');

      dispatch('notify/loadNotifications', undefined, { root: true });

      Router.push({ name: 'home' });
    })
    .catch(error => {
      dispatch('root/handleRequestError', {
        error,
        title: 'Can not log in.',
        text: 'Check if email and/or password are correct.'
      }, { root: true });
    })
    .finally(() => commit('loading', false));
}

// delete token and clear store
export const logout = ({ rootState, commit }) => {
  rootState.api.auth.logout()
    .then(() => {})
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not logout.<br>Check internet connection'
      });
    })
    .finally(() => {
      commit('setToken', null);
      window.location.reload();
    })
}

// load logged user permissions
export const loadPermissions = ({ rootState, commit, dispatch }) => {
  rootState.api.auth.getPermissions()
    .then(out => {
      commit('clearPermissions');
      out.data.result.forEach(e => {
        commit('addPermission', new Permission(e.id, e.slug));
      });
    })
    .catch(error => {
      dispatch('root/handleRequestError', {
        error,
        title: 'User permissions problem',
        text: 'Can not load logged in user set of his permissions.'
      }, { root: true });
    });
}

// load informations about logged user
export const loadUserInfo = ({ rootState, commit }) => {
  rootState.api.auth.getUserInfo()
    .then(out => {
      const data = out.data.result;

      const auth = new Auth(
        new User(
          data.id,
          data.first_name,
          data.last_name,
          data.email,
          data.phone,
          !!data.status.active
        )
      );

      commit('setUserInfo', auth);
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load user informations.<br>Check internet connection.'
      })
    })
}

// change users password
export const changePassword = ({ rootState, state, commit }, { route = null, token = null }) => {
  rootState.api.auth.changePassword(state.newPasswordForm.password, token)
    .then(() => {
      Vue.notify({
        group: 'app',
        type: 'success',
        text: 'Successfully changed password.'
      })
      if (state.requirePasswordChange) {
        commit('requirePasswordChange', false);
      }

      if (route) {
        setTimeout(() => {
          Router.push({ name: route });
        }, 750);
      }
    })
    .catch(error => {
      console.error(error);
      if (error.response.data.response === 'New password can not be the same as current one') {
        Vue.notify({
          group: 'app',
          type: 'error',
          text: 'Password is the same as current one.'
        });
      } else {
        Vue.notify({
          group: 'app',
          type: 'error',
          text: 'Can not change password.<br>Check internet connection.'
        });
      }
    })
}

// check if password reset token is still valid
export const checkToken = ({ rootState }, token) => {
  rootState.api.auth.checkPasswordResetToken(token)
    .then(() => {})
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Password reset token expired'
      });
      Router.push({ name: 'page-login' });
    })
}

// send email with link to reset forgotten password
export const sendPasswordResetLink = ({ rootState, state, commit }) => {
  const email = state.forgotPasswordEmail;

  commit('resetPasswordLock', true);
  rootState.api.auth.resetPassword(email)
    .then(() => {
      commit('insertEmail', '');
      commit('resetPasswordSend', true);
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not send message with link to reset password.<br>Check internet connection.'
      });
    })
    .finally(() => {
      commit('resetPasswordLock', false);
    })
}
