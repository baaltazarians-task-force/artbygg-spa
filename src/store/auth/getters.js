export const getToken = (state) => state.token;
export const getUserGroups = (state) => state.groups;
export const getUserPermissions = (state) => state.permissions;

export const getTokenPayload = (_, getters) => {
  const token = getters.getToken;
  if (token && typeof token === 'string') {
    const payloadRaw = token.split('.')[1];
    return JSON.parse(window.atob(payloadRaw));
  }
  return null;
}
