export const loading = (state, loading = true) => state.loading = loading;

export const setToken = (state, token) => state.token = token;
export const editLoginForm = (state, { key, value }) => state.loginForm[key] = value;
export const editNewPasswordForm = (state, { key, value }) => state.newPasswordForm[key] = value;

export const clearPermissions = (state) => state.permissions = [];
export const clearGroups = (state) => state.groups = [];

export const addPermission = (state, permission) => state.permissions.push(permission);
export const addGroup = (state, group) => state.groups.push(group);

export const setUserInfo = (state, auth) => state.auth = auth;
export const requirePasswordChange = (state, flag) => state.requirePasswordChange = flag;

export const insertEmail = (state, email) => state.forgotPasswordEmail = email;
export const resetPasswordSend = (state, flag) => state.resetPasswordSend = flag;
export const resetPasswordLock = (state, flag) => state.resetPasswordLock = flag;
