export const notifySuccess = ({ state }, { title, text }) => {
  state.vs.notify({
    title,
    text,
    color: 'success'
  });
}

export const notifyWarning = ({ state }, { title, text }) => {
  state.vs.notify({
    title,
    text,
    color: 'warning'
  });
}

export const notifyError = ({ state }, { title, text }) => {
  state.vs.notify({
    title,
    text,
    color: 'danger'
  });
}

export const handleRequestError = ({ dispatch }, { error, title, text }) => {
  console.error(error);
  dispatch('notifyError', { title, text });
}
