import state from "./state";

export const addUser    = (state, user) => state.users.push(user);
export const clearUsers = (state) => state.users = [];

export const setCurrentPage = (state, page) => state.currentPage = page;
export const setMaxPage     = (state, page) => state.maxPage = page;
export const search         = (state, search) => state.search = search;

export const editFilter  = (state, { key, value }) => state.filter[key] = value;
export const resetFilter = (state) => Object.keys(state.filter).forEach(k => state.filter[k] = null);

export const editUserForm = (state, { key, value }) => state.newUser[key] = value;
export const clearUserForm = (state) => {
  state.newUser.id        = 0;
  state.newUser.firstName = '';
  state.newUser.lastName  = '';
  state.newUser.email     = '';
  state.newUser.phone     = '';
  state.newUser.company   = null;
  state.newUser.group     = null;
  state.newUser.workPlace = null;
  state.newUser.status    = false;
}

export const aboutUser = (state, userId) => state.aboutUser = userId;