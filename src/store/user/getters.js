export const getSelectStatus = () => {
  return [
    { label: 'Inactive', value: false },
    { label: 'Active', value: true },
  ]
}

export const getUserById = (state) => (userId) => {
  return state.users.find(u => u.id == userId) || null;
}
