export default {
  users: [],
  selected: [],
  currentPage: 1,
  maxPage: 0,
  limit: 30,
  loading: false,
  search: '',
  aboutUser: 0,
  newUser: {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    company: null,
    group: null,
    workPlace: null,
    status: false,
  },
  filter: {
    userGroup: null,
    company: null,
    status: null
  }
}
