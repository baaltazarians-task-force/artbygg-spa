import Vue from 'vue'
import _User from '@/classes/User/User'

export const loadUsers = ({ rootState, state, commit }) => {
  commit('clearUsers');

  const currentPage = state.currentPage;
  const limit       = state.limit;
  const search      = state.search;
  const filter      = Object.keys(state.filter).filter(k => state.filter[k]).map(k => `${k}.${state.filter[k].value}`)

  rootState.api.user.getUsers(currentPage, limit, search, [], [], filter)
    .then(out => {
      const resp = out.data.response;
      commit('setMaxPage', resp.max_pages);
      resp.users.forEach(e => commit(
        'addUser',
        new _User(
          e.id,
          e.first_name,
          e.last_name,
          e.email,
          e.company_id,
          e.groupsIds,
          e.status ? e.status.active : null
        )
      ));
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load user list',
      });
    });
}

export const createUser = ({ rootState, state, commit }) => {
  const form = state.newUser;
  const data = {
    first_name:       form.firstName,
    last_name:        form.lastName,
    email_address:    form.email,
    phone:            form.phone.replace(/ /g, ''),
    company_id:       form.company.value,
    work_position_id: form.workPlace.value,
    activate:         form.status,
    group_id:         form.group.value
  }

  rootState.api.user.createUser(data)
    .then(() => {
      commit('clearUserForm');
      Vue.notify({
        group: 'app',
        type: 'success',
        text: 'Successfully added new user'
      });
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not add new user.<br>Check internet connection.'
      })
    })
}

export const changeStatus = ({ rootState, dispatch }, { userId, status }) => {
  rootState.api.user.changeStatus(userId, status)
    .then(out => {
      dispatch('loadUsers');
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not change user status.<br>Check internet connection'
      })
    })
}