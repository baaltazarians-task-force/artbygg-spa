

  // /////////////////////////////////////////////
  // COMPONENTS
  // /////////////////////////////////////////////

  // Vertical NavMenu
  export const updateVerticalNavMenuWidth = ({ commit }, width) => {
    commit('UPDATE_VERTICAL_NAV_MENU_WIDTH', width)
  }

  // VxAutoSuggest
  export const updateStarredPage = ({ commit }, payload) => {
    commit('UPDATE_STARRED_PAGE', payload)
  }

  // The Navbar
  export const arrangeStarredPagesLimited = ({ commit }, list) => {
    commit('ARRANGE_STARRED_PAGES_LIMITED', list)
  }
  export const arrangeStarredPagesMore = ({ commit }, list) => {
    commit('ARRANGE_STARRED_PAGES_MORE', list)
  }

  // /////////////////////////////////////////////
  // UI
  // /////////////////////////////////////////////

  export const toggleContentOverlay = ({ commit }) => {
    commit('TOGGLE_CONTENT_OVERLAY')
  }
  export const updateTheme = ({ commit }, val) => {
    commit('UPDATE_THEME', val)
  }

  // /////////////////////////////////////////////
  // User/Account
  // /////////////////////////////////////////////

  export const updateUserInfo = ({ commit }, payload) => {
    commit('UPDATE_USER_INFO', payload)
  }

