import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


// import modules
import api from './api'
import auth from './auth'
import root from './root'
import user from './user'
import group from './group'
import notify from './notify'
import company from './company'
import framework from './framework'
import permission from './permission'

export default new Vuex.Store({
    modules: {
        api,
        auth,
        root,
        user,
        group,
        notify,
        company,
        framework,
        permission
    },
    strict: false
});

