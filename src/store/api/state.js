import axios from 'axios'
import store from '../index'

import AuthService from '../../services/AuthService'
import UserService from '../../services/UserService'
import GroupService from '../../services/GroupService'
import NotifyService from '../../services/NotifyService'
import CompanyService from '../../services/CompanyService'
import PermissionService from '../../services/PermissionService'

// set hostname base
const hostname = 'http://localhost:8000/v1';

// add token to header of every request
axios.interceptors.request.use(config => {
    const token = store.getters['auth/getToken'];
    config.headers['Authorization'] = `Bearer ${token}`;

    return config;
    
});

export default {
    auth: new AuthService(axios, hostname),
    user: new UserService(axios, hostname),
    group: new GroupService(axios, hostname),
    notify: new NotifyService(axios, hostname),
    company: new CompanyService(axios, hostname),
    permission: new PermissionService(axios, hostname)
}
