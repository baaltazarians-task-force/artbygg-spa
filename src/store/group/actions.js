import Vue from 'vue'
import Group from '@/classes/Group/Group';

export const loadGroups = ({ rootState, state, commit }, loadPermissions = true) => {
  commit('clearGroups');

  const currentPage = state.currentPage;
  const limit       = state.limit;
  const search      = state.search;

  rootState.api.group.getGroups(currentPage, limit, search, [], ['permissions'])
    .then(out => {
      const resp = out.data.response;

      commit('setMaxPage', resp.max_pages);
      resp.groups.forEach(e => commit('addGroup', new Group(e.id, e.slug, e.description || null, e.permissions)));
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not load group list',
      });
    });
}

export const createGroup = ({ rootState, state, commit }) => {
  const form = state.newGroup;
  const data = {
    slug: form.slug,
    description: form.description,
    add_permissions: {}
  };

  const perm = form.permissions;
  const keys = Object.keys(perm);

  keys
    .filter(e => perm[e].c || perm[e].r || perm[e].u || perm[e].d || perm[e].o)
    .forEach(e => data.add_permissions[perm[e]._id] = {
      create: perm[e].c ? 1 : 0,
      read:   perm[e].r ? 1 : 0,
      update: perm[e].u ? 1 : 0,
      delete: perm[e].d ? 1 : 0,
      other:  perm[e].o ? 1 : 0
    });

  rootState.api.group.createGroup(data)
    .then(out => {
      const resp = out.data.response.group;
      commit('addGroup', new Group(resp.id, resp.slug, resp.description || null));
      commit('clearForm');
      commit('editGroupModal', { key: 'modal', value: false });
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not create new group<br>Check internet connection'
      })
    })
}

// TO CHANGE !!!
export const saveGroup = ({ rootState, state, dispatch, getters, commit }) => {
  const form = state.newGroup;
  const originalGroup = getters.getGroupById(form.id);
  const data = {
    slug: form.slug,
    description: form.description,
    add_permissions: {},
    remove_permissions: [],
  };

  const perm = form.permissions;
  const oldPerm = originalGroup.permissions;
  const newKeys = Object.keys(perm).filter(e =>
    perm[e].c || perm[e].r || perm[e].u || perm[e].d || perm[e].o
  );

  oldPerm.forEach(e => data.remove_permissions.push(e.id));

  newKeys
    .forEach(e => data.add_permissions[perm[e]._id] = {
      create: perm[e].c ? 1 : 0,
      read:   perm[e].r ? 1 : 0,
      update: perm[e].u ? 1 : 0,
      delete: perm[e].d ? 1 : 0,
      other:  perm[e].o ? 1 : 0
    });

  rootState.api.group.updateGroup(data, originalGroup.id)
    .then(() => {
      dispatch('loadGroups');
      commit('editGroupModal', { key: 'modal', value: false });
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Can not save changes made to this group.<br> Check internet connection.'
      })
    })
}

export const deleteSelectedGroups = ({ rootState, state, commit, dispatch }, groupId = false) => {
  console.log(groupId);

  Promise.all(
    groupId ?
      [rootState.api.group.deleteGroup(groupId)]
      : state.selected.map(e => rootState.api.group.deleteGroup(e))
  )
    .then(() => {
      commit('unselectAll');
      dispatch('loadGroups');
      Vue.notify({
        group: 'app',
        type: 'success',
        text: 'Successfully deleted selected groups'
      })
    })
    .catch(error => {
      console.error(error);
      Vue.notify({
        group: 'app',
        type: 'error',
        text: 'Something went wrong while deleting selected groups'
      });
    })
}

export const editElement = async ({ rootState, state, commit, getters }, groupId) => {
  commit('clearForm');

  const group = getters.getGroupById(groupId);
  const permissionKeys = {
    create: 'c',
    read:   'r',
    update: 'u',
    delete: 'd',
    other:  'o',
  }
  
  commit('editGroupModal', { key: 'id', value: groupId });
  commit('editGroupModal', { key: 'slug', value: group.slug });
  commit('editGroupModal', { key: 'description', value: group.description });

  commit('editGroupModal', { key: 'modal', value: true });
  setTimeout(() => {
    group.permissions.forEach(p => {
      Object.keys(permissionKeys).forEach(pk => {
        commit('modifyPermission', { key: p.slug, field: permissionKeys[pk], flag: !!p[pk] });
      });
    })
  }, 150);
}
