export default {
  groups: [],
  selected: [],
  currentPage: 1,
  maxPage: 0,
  limit: 30,
  loading: false,
  search: '',
  newGroup: {
    id: 0,
    modal: false,
    slug: '',
    description: '',
    permissions: {}
  },
}
