export const getGroupById = (state) => (groupId) => {
  return state.groups.find(g => g.id === groupId) || null;
}

export const getSelectGroups = (state) => {
  return state.groups.map(g => {
    return {
      label: g.slug,
      value: g.id
    }
  })
}