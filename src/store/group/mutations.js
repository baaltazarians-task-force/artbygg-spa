import Vue from 'vue';

export const addGroup    = (state, group) => state.groups.push(group);
export const clearGroups = (state) => state.groups = [];

export const setCurrentPage = (state, page) => state.currentPage = page;
export const setMaxPage     = (state, page) => state.maxPage = page;
export const search         = (state, search) => state.search = search;

export const editGroupModal = (state, { key, value }) => state.newGroup[key] = value;
export const attachPermission = (state, { key, _id, c, r, u, d, o }) => {
  Vue.set(state.newGroup.permissions, key, { _id, c, r, u, d, o });
}
export const modifyPermission = (state, { key, field, flag }) => {
  state.newGroup.permissions[key][field] = flag;
}

export const clearForm = (state) => {
  state.newGroup.id = 0,
  state.newGroup.slug = '';
  state.newGroup.description = '';

  // clear states
  const perm = state.newGroup.permissions;
  const fieldsToClear = ['c', 'r', 'u', 'd', 'o'];

  Object.keys(perm).forEach(k => {
    fieldsToClear.forEach(f => {
      perm[k][f] = false;
    })
  });
}

export const selectGroup = (state, groupId) => state.selected.push(groupId);
export const unselectGroup = (state, groupId) => state.selected.splice(state.selected.indexOf(groupId), 1);
export const selectAll = (state) => state.groups.forEach(e => state.selected.push(e.id));
export const unselectAll = (state) => state.selected = [];
